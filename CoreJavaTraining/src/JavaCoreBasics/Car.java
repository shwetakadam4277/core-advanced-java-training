package JavaCoreBasics;

import java.util.Scanner;

public class Car {
	
	 protected static String CarModel;
	 protected static double CarPrice;
	 protected static String CarType;
	 protected static int InsurranceType;
	 static int Insurrance;
	 
	 public Car(String CarModel,double CarPrice,String CarType,int InsurranceType) {
		 this.CarModel=CarModel;
		 this.CarPrice=CarPrice;
		 this.CarType=CarType;
		 this.InsurranceType=InsurranceType;
	 }
	 public void setCarModel(String CarModel) {
		 this.CarModel=CarModel;
	 }
	 public void setCarPrice(double CarPrice) {
		 this.CarPrice=CarPrice;
	 }
	 public void setCarType(String CarType) {
		 this.CarType=CarType;
	 }
	 public void setInsurranceType(int InsurranceType) {
		 this.InsurranceType=InsurranceType;
	 }
	 
	 
	 public static String getCarModel() {
		 return CarModel;
	 }
	 public String getCarType() {
		 return CarType;
	 }
	 public double getCarPrice() {
		 return CarPrice;
	 }
	 public int getInsurranceType() {
		 return InsurranceType;
	 }
	 
	 public static void DisplayData() {

		 if(InsurranceType==1) {
				System.out.println("Details of Car: ");
				System.out.println("Name:"+CarModel);
				System.out.println("Price:"+CarPrice);
				System.out.println("The insurrance of"+CarModel+" is:"+Insurrance);
			}else {
				Insurrance=Insurrance+((20*Insurrance)/100);
				System.out.println("Details of Car: ");
				System.out.println("Name:"+CarModel);
				System.out.println("Price:"+CarPrice);
			System.out.println("The insurrance of"+CarModel+" is:"+Insurrance);
			}
	 }
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		char AddMoreCar;
		
		

		
		do {
		System.out.println("Enter the Model of Car:");
		String CarModel=sc.next(); 
		
		System.out.println("Enter the Price of Car:");
		double CarPrice=sc.nextDouble();
		
		System.out.println("Enter the type of Car:");
		System.out.println("1.HatchBack  2.Sedan  3.SUV");
		String CarType=sc.next();
		
		Insurrance=0;
		
		switch(CarType) {
		
		case "1":
			Insurrance=(int) ((5*CarPrice)/100);
			break;
			
		case "2":
			Insurrance=(int)((8*CarPrice)/100);
			break;
			
		case "3":
			Insurrance=(int)((10*CarPrice)/100);
			break;
			
		default:
			System.out.println("Car type Entered is not Valid");
		}

			System.out.println("Enter type of insurrance 1.Basic 2.Premium");
			int InsurranceType=sc.nextInt();
			
			Car vehicle=new Car(CarModel,CarPrice,CarType,InsurranceType);
			vehicle.DisplayData();
			
			
			System.out.println("Do you want to enter details of any other car (y/n):");
			 AddMoreCar=sc.next().charAt(0);
			}
			while(AddMoreCar!='n');
		
		
		sc.close();

	}

}
